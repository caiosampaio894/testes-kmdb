from django.http import response
from django.test import TestCase
from django.contrib.auth.models import User
from .models import Film, Genre, Movie, Review
from rest_framework.test import APITestCase
from rest_framework import status
from rest_framework.authtoken.models import Token
from .serializers import FilmSerializer

class FilmTestModel(TestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        cls.title = 'O Poderoso Chefão 2',
        cls.duration = '175m',
        cls.genres = [],
        cls.premiere = '1972-09-10',
        cls.classification = 14,
        cls.synopsis = 'Don Vito Corleone (Marlon Brando) é o chefe de uma "família" ...'

        cls.movie = Movie.objects.create(
            title=cls.title,
            duration=cls.duration,
            genres=cls.genres,
            premiere=cls.premiere,
            classification=cls.classification,
            synopsis=cls.synopsis
        )

        cls.list_genre = [{"name": "Crime"}, {"name": "Drama"}, {"name": "Mafia"}]

        for genre in cls.list_genre:
            cls.genre = Genre.objects.create(name=genre.name, movie=cls.movie)


    def test_movie_fieldsl(self):
        self.assertIsInstance(self.movie.title, str)
        self.assertEqual(self.movie.title, self.title)

        self.assertIsInstance(self.movie.duration, str)
        self.assertEqual(self.movie.duration, self.duration)

        self.assertIsInstance(self.movie.genres, list)
        self.assertEqual(self.movie.genres, self.list_genre)

        self.assertIsInstance(self.movie.premiere, str)
        self.assertEqual(self.movie.premiere, self.premiere)

        self.assertIsInstance(self.movie.classification, int)
        self.assertEqual(self.movie.classification, self.classification)

        self.assertIsInstance(self.movie.synopsis, str)
        self.assertEqual(self.movie.synopsis, self.synopsis)

    def test_movie_genre_relationship(self):
        genres = [Genre.objects.create(name=f'Genre {i}') for i in range(3)]

        for genre in genres:
            self.movie.genres.add(genre)

        self.assertEqual(len(genres), self.movie.genres.count() - self.list_genre.count())

        for genre in genres:
            self.assertIn(genre, self.movie.genres)


        

class MovieViewTest(APITestCase):
    def test_list_all_movies(self):
        movie = Movie.objects.create(
            title = 'O Poderoso Chefão 2',
            duration = '175m',
            genres = [],
            premiere = '1972-09-10',
            classification = 14,
            synopsis = 'Don Vito Corleone (Marlon Brando) é o chefe de uma "família" ...'
        )

        response = self.client.get('/api/movies/')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(len(response.json()), Movie.objects.count())


    def test_create_movie(self):
        admin = User.objects.create_superuser(
            username = 'admin',
            password = '1234',
            first_name = 'John',
            last_name = 'Cena'
        )

        token = Token.objects.create(user=admin)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token.key}')

        movie_data = {
            'title': 'O Poderoso Chefão 2',
            'duration': '175m',
            'genres': [
                {'name': 'Crime'},
                {'name': 'Drama'}
            ],
            'premiere': '1972-09-10',
            'classification': 14,
            'synopsis': 'Don Vito Corleone (Marlon Brando) é o chefe de uma "família" ...'
        }

        response = self.client.post('/api/movies/', movie_data)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        self.assertIn('id', response.json())

        self.assertEqual(response.json().get('title'), movie_data['title'])


    def test_critic_cannot_create_movie(self):
        critic = User.objects.create_user(
            username = 'admin',
            password = '1234',
            first_name = 'John',
            last_name = 'Cena',
            is_superuser = False,
            is_staff = True
        )

        token = Token.objects.create(user=critic)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token.key}')

        movie_data = {
            'title': 'O Poderoso Chefão 2',
            'duration': '175m',
            'genres': [
                {'name': 'Crime'},
                {'name': 'Drama'}
            ],
            'premiere': '1972-09-10',
            'classification': 14,
            'synopsis': 'Don Vito Corleone (Marlon Brando) é o chefe de uma "família" ...'
        }

        response = self.client.post('/api/movies/', movie_data)

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


    def test_create_movie_fail(self):
        admin = User.objects.create_superuser(
            username = 'admin',
            password = '1234',
            first_name = 'John',
            last_name = 'Cena'
        )

        token = Token.objects.create(user=admin)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token.key}')

        movie_data = {
            'duration': '175m',
            'genres': [
                {'name': 'Crime'},
                {'name': 'Drama'}
            ],
            'premiere': '1972-09-10',
            'classification': 14,
            'synopsis': 'Don Vito Corleone (Marlon Brando) é o chefe de uma "família" ...'
        }

        response = self.client.post('/api/movies/', movie_data)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


    def test_update_movie(self):
        admin = User.objects.create_superuser(
            username = 'admin',
            password = '1234',
            first_name = 'John',
            last_name = 'Cena'
        )

        token = Token.objects.create(user=admin)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token.key}')

        movie = Movie.objects.create(
            title = 'O Poderoso Chefão 3',
            duration = '175m',
            genres = [],
            premiere = '1972-09-10',
            classification = 14,
            synopsis = 'Don Vito Corleone (Marlon Brando) é o chefe de uma "família" ...'
        )

        movie_data = {
            'title': 'O Poderoso Chefão 2',
            'duration': '175m',
            'genres': [
                {'name': 'Crime'},
                {'name': 'Drama'},
                {'name': 'Mafia'}
            ],
            'premiere': '1972-09-10',
            'classification': 14,
            'synopsis': 'Don Vito Corleone (Marlon Brando) é o chefe de uma "família" ...'
        }

        response = self.client.put(f'/api/movies/{movie.id}/', movie_data)

        movie.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for k, v in movie_data.items():
            self.assertEquals(v, response.data[k])
            self.assertEquals(v, getattr(movie, k))


    def test_update_movie_invalid_id(self):
        admin = User.objects.create_superuser(
            username = 'admin',
            password = '1234',
            first_name = 'John',
            last_name = 'Cena'
        )

        token = Token.objects.create(user=admin)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token.key}')

        movie = Movie.objects.create(
            title = 'O Poderoso Chefão 3',
            duration = '175m',
            genres = [],
            premiere = '1972-09-10',
            classification = 14,
            synopsis = 'Don Vito Corleone (Marlon Brando) é o chefe de uma "família" ...'
        )

        movie_data = {
            'title': 'O Poderoso Chefão 2',
            'duration': '175m',
            'genres': [
                {'name': 'Crime'},
                {'name': 'Drama'},
                {'name': 'Mafia'}
            ],
            'premiere': '1972-09-10',
            'classification': 14,
            'synopsis': 'Don Vito Corleone (Marlon Brando) é o chefe de uma "família" ...'
        }

        response = self.client.put(f'/api/movies/99/', movie_data)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


    def test_update_movie_invalid_token(self):
        user = User.objects.create_user(
            username = 'user',
            password = '1234',
            first_name = 'John',
            last_name = 'Cena'
        )

        token = Token.objects.create(user=user)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token.key}')

        movie = Movie.objects.create(
            title = 'O Poderoso Chefão 3',
            duration = '175m',
            genres = [],
            premiere = '1972-09-10',
            classification = 14,
            synopsis = 'Don Vito Corleone (Marlon Brando) é o chefe de uma "família" ...'
        )

        movie_data = {
            'title': 'O Poderoso Chefão 2',
            'duration': '175m',
            'genres': [
                {'name': 'Crime'},
                {'name': 'Drama'},
                {'name': 'Mafia'}
            ],
            'premiere': '1972-09-10',
            'classification': 14,
            'synopsis': 'Don Vito Corleone (Marlon Brando) é o chefe de uma "família" ...'
        }

        response = self.client.put(f'/api/movies/{movie.id}/', movie_data)

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


    def test_update_movie_missing_arguments(self):
        admin = User.objects.create_superuser(
            username = 'admin',
            password = '1234',
            first_name = 'John',
            last_name = 'Cena'
        )

        token = Token.objects.create(user=admin)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token.key}')

        movie = Movie.objects.create(
            title = 'O Poderoso Chefão 3',
            duration = '175m',
            genres = [],
            premiere = '1972-09-10',
            classification = 14,
            synopsis = 'Don Vito Corleone (Marlon Brando) é o chefe de uma "família" ...'
        )

        movie_data = {
            'duration': '175m',
            'genres': [
                {'name': 'Crime'},
                {'name': 'Drama'},
                {'name': 'Mafia'}
            ],
            'classification': 14,
            'synopsis': 'Don Vito Corleone (Marlon Brando) é o chefe de uma "família" ...'
        }

        response = self.client.put(f'/api/movies/{movie.id}/', movie_data)

        movie.refresh_from_db()

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


    def test_delete_movie(self):
        admin = User.objects.create_superuser(
            username = 'admin',
            password = '1234',
            first_name = 'John',
            last_name = 'Cena'
        )

        token = Token.objects.create(user=admin)

        self.client.credentials(HTTP_AUTHORIZATION=f'Token {token.key}')

        movie = Movie.objects.create(
            title = 'O Poderoso Chefão 3',
            duration = '175m',
            genres = [],
            premiere = '1972-09-10',
            classification = 14,
            synopsis = 'Don Vito Corleone (Marlon Brando) é o chefe de uma "família" ...'
        )

        response = self.client.delete(f'/api/movies/{movie.id}/')

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


    class GenreModelTest(TestCase):
        @classmethod
        def setUpTestData(cls) -> None:
            cls.name = 'Máfia',

            cls.genre = Genre.objects.create(
                name=cls.name
            )


        def test_genre_fields(self):
            self.assertIsInstance(self.genre.name, str)
            self.assertEqual(self.genre.name, self.name)

    class ReviewModelTest(TestCase):
        @classmethod
        def setUpTestData(cls) -> None:
            cls.username = 'user'
            cls.password = '1234'
            cls.first_name = 'John'
            cls.last_name = 'Wick'
            cls.is_superuser = False
            cls.is_staff = True
            
            cls.user = User.objects.create_user(
                username=cls.username,
                password=cls.password,
                first_name=cls.first_name,
                last_name=cls.last_name,
                is_superuser=cls.is_superuser,
                is_staff=cls.is_staff
            )

            cls.stars = 7,
            cls.review = 'O Poderoso Chefão 2 podia ter dado muito errado...',
            cls.spoilers = False,

            cls.review = Review.objects.create(
                critic = cls.user,
                stars=cls.stars,
                review=cls.review,
                spoilers=cls.spoilers
            )


        def test_review_fields(self):
            self.assertIsInstance(self.review.critic, User)
            self.assertEqual(self.review.critic.username, self.username)

            self.assertIsInstance(self.review.stars, int)
            self.assertEqual(self.review.stars, self.stars)

            self.assertIsInstance(self.review.review, str)
            self.assertEqual(self.review.review, self.review)

            self.assertIsInstance(self.review.spoilers, bool)
            self.assertEqual(self.review.spoilers, self.spoilers)


        def test_review_critic_relationship(self):
            reviews = [Review.objects.create(
                critic=self.user,
                stars=i + 1,
                review=f'Review {i}',
                spoilers=False
            ) for i in range(3)]

            for review in reviews:
                self.assertEqual(review.critic, self.user)


        def test_review_movie_relationship(self):
            movie = Movie.objects.create(
                title='O Poderoso Chefão 2',
                duration='175m',
                genres=[],
                premiere='1972-09-10',
                classification=14,
                synopsis='Don Vito Corleone (Marlon Brando) é o chefe de uma "família" ...'
            )

            reviews = [Review.objects.create(
                critic=self.user,
                stars=i + 1,
                review=f'Review {i}',
                spoilers=False
            ) for i in range(3)]

            for review in reviews:
                movie.reviews.add(review)

            self.assertEqual(len(reviews), movie.reviews.count())

            for review in reviews:
                self.assertIn(review, movie.reviews)
    
    class ReviewViewTest(APITestCase):
        def test_create_review(self):
            critic = User.objects.create_user(
                username = 'critic',
                password = '1234',
                first_name = 'John',
                last_name = 'Cena',
                is_superuser = False,
                is_staff = True
            )

            token = Token.objects.create(user=critic)

            self.client.credentials(HTTP_AUTHORIZATION=f'Token {token.key}')

            movie = Movie.objects.create(
                title = 'O Poderoso Chefão 3',
                duration = '175m',
                genres = [
                    {'name': 'Crime'},
                    {'name': 'Drama'},
                    {'name': 'Mafia'},
                ],
                premiere = '1972-09-10',
                classification = 14,
                synopsis = 'Don Vito Corleone (Marlon Brando) é o chefe de uma "família" ...'
            )

            review_data = {
                    'stars': 7,
                    'review': 'O Poderoso Chefão 2 podia ter dado muito errado...',
                    'spoilers': False,
            }

            response = self.client.post(f'/api/movies/{movie.id}/review/', review_data)

            self.assertEqual(response.status_code, status.HTTP_201_CREATED)

            self.assertIn('id', response.json())

            self.assertEqual(response.json().get('stars'), review_data['stars'])


        def test_user_cannot_create_review(self):
            user = User.objects.create_user(
                username = 'user',
                password = '1234',
                first_name = 'John',
                last_name = 'Romero',
            )

            token = Token.objects.create(user=user)

            self.client.credentials(HTTP_AUTHORIZATION=f'Token {token.key}')

            movie = Movie.objects.create(
                title = 'O Poderoso Chefão 3',
                duration = '175m',
                genres = [
                    {'name': 'Crime'},
                    {'name': 'Drama'},
                    {'name': 'Mafia'},
                ],
                premiere = '1972-09-10',
                classification = 14,
                synopsis = 'Don Vito Corleone (Marlon Brando) é o chefe de uma "família" ...'
            )

            review_data = {
                    'stars': 7,
                    'review': 'O Poderoso Chefão 2 podia ter dado muito errado...',
                    'spoilers': False,
            }

            response = self.client.post(f'/api/movies/{movie.id}/review/', review_data)

            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


        def test_create_review_fail(self):
            critic = User.objects.create_user(
                username = 'critic',
                password = '1234',
                first_name = 'John',
                last_name = 'Cena',
                is_superuser = False,
                is_staff = True
            )

            token = Token.objects.create(user=critic)

            self.client.credentials(HTTP_AUTHORIZATION=f'Token {token.key}')

            movie = Movie.objects.create(
                title = 'O Poderoso Chefão 3',
                duration = '175m',
                genres = [
                    {'name': 'Crime'},
                    {'name': 'Drama'},
                    {'name': 'Mafia'},
                ],
                premiere = '1972-09-10',
                classification = 14,
                synopsis = 'Don Vito Corleone (Marlon Brando) é o chefe de uma "família" ...'
            )

            review_data = {
                    'review': 'O Poderoso Chefão 2 podia ter dado muito errado...',
                    'spoilers': False,
            }

            response = self.client.post(f'/api/movies/{movie.id}/review/', review_data)

            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


        def test_list_reviews_critic(self):
            critic = User.objects.create_user(
                username = 'critic',
                password = '1234',
                first_name = 'John',
                last_name = 'Cena',
                is_superuser = False,
                is_staff = True
            )

            token = Token.objects.create(user=critic)

            self.client.credentials(HTTP_AUTHORIZATION=f'Token {token.key}')

            movie1 = Movie.objects.create(
                title = 'O Poderoso Chefão 3',
                duration = '175m',
                genres = [
                    {'name': 'Crime'},
                    {'name': 'Drama'},
                    {'name': 'Mafia'},
                ],
                premiere = '1972-09-10',
                classification = 14,
                synopsis = 'Don Vito Corleone (Marlon Brando) é o chefe de uma "família" ...'
            )

            review1 = Review.objects.create(
                critic = critic,
                stars=10,
                review='Um clássico ...',
                spoilers=False
            )

            movie1.reviews.add(review1)

            movie2 = Movie.objects.create(
                title = 'Pulp Fiction',
                duration = '140m',
                genres = [
                    {'name': 'Hard Core'},
                ],
                premiere = '1994-09-10',
                classification = 16,
                synopsis = 'Os caminhos de vários criminosos se cruzam nestas três histórias ...'
            )

            review2 = Review.objects.create(
                critic = critic,
                stars=10,
                review='Um baita filme ...',
                spoilers=False
            )

            movie2.reviews.add(review2)

            response = self.client.get('/api/reviews/')

            self.assertEqual(response.status_code, status.HTTP_200_OK)

            self.assertEqual(len(response.json()), Review.objects.count())

            self.assertEqual(movie2.reviews[0]['critic'], critic)

        def test_list_reviews_admin(self):
            admin = User.objects.create_superuser(
                username = 'admin',
                password = '1234',
                first_name = 'John',
                last_name = 'Cena'
            )

            token = Token.objects.create(user=admin)

            self.client.credentials(HTTP_AUTHORIZATION=f'Token {token.key}')

            critic1 = User.objects.create_user(
                username = 'critic1',
                password = '1234',
                first_name = 'John',
                last_name = 'Romero',
                is_superuser = False,
                is_staff = True
            )

            critic2 = User.objects.create_user(
                username = 'critic2',
                password = '1234',
                first_name = 'John',
                last_name = 'Deere',
                is_superuser = False,
                is_staff = True
            )

            movie = Movie.objects.create(
                title = 'O Poderoso Chefão 3',
                duration = '175m',
                genres = [
                    {'name': 'Crime'},
                    {'name': 'Drama'},
                    {'name': 'Mafia'},
                ],
                premiere = '1972-09-10',
                classification = 14,
                synopsis = 'Don Vito Corleone (Marlon Brando) é o chefe de uma "família" ...'
            )

            review1 = Review.objects.create(
                critic = critic1,
                stars = 10,
                review = 'Um baita filme ...',
                spoilers = False
            )

            review2 = Review.objects.create(
                critic = critic2,
                stars = 9,
                review = 'Um ótimo filme ...',
                spoilers = True
            )

            movie.reviews.add(review1, review2)

            response = self.client.get('/api/reviews/')

            self.assertEqual(response.status_code, status.HTTP_200_OK)

            self.assertEqual(len(response.json()), Review.objects.count())


        def test_update_review(self):
            critic = User.objects.create_user(
                username = 'critic',
                password = '1234',
                first_name = 'John',
                last_name = 'Cena',
                is_superuser = False,
                is_staff = True
            )

            token = Token.objects.create(user=critic)

            self.client.credentials(HTTP_AUTHORIZATION=f'Token {token.key}')

            movie = Movie.objects.create(
                title = 'O Poderoso Chefão 3',
                duration = '175m',
                genres = [
                    {'name': 'Crime'},
                    {'name': 'Drama'},
                    {'name': 'Mafia'},
                ],
                premiere = '1972-09-10',
                classification = 14,
                synopsis = 'Don Vito Corleone (Marlon Brando) é o chefe de uma "família" ...'
            )

            review = Review.objects.create(
                    stars = 7,
                    review = 'O Poderoso Chefão 2 podia ter dado muito errado...',
                    spoilers = False,
                    critic = critic
            )

            review_data = {
                'stars': 10,
                'review': 'O Poderoso Chefão 2 podia ter dado muito certo...',
                'spoilers': True,
            }

            response = self.client.put(f'/api/movies/{movie.id}/review/', review_data)

            review.refresh_from_db()

            self.assertEqual(response.status_code, status.HTTP_200_OK)

            for k, v in review_data.items():
                self.assertEquals(v, response.data[k])
                self.assertEquals(v, getattr(review, k))


        def test_update_review_invalid_token(self):
            user = User.objects.create_user(
                username = 'user',
                password = '1234',
                first_name = 'John',
                last_name = 'Cena'
            )

            token = Token.objects.create(user=user)

            self.client.credentials(HTTP_AUTHORIZATION=f'Token {token.key}')

            movie = Movie.objects.create(
                title = 'O Poderoso Chefão 3',
                duration = '175m',
                genres = [
                    {'name': 'Crime'},
                    {'name': 'Drama'},
                    {'name': 'Mafia'},
                ],
                premiere = '1972-09-10',
                classification = 14,
                synopsis = 'Don Vito Corleone (Marlon Brando) é o chefe de uma "família" ...'
            )

            review = Review.objects.create(
                    stars = 7,
                    review = 'O Poderoso Chefão 2 podia ter dado muito errado...',
                    spoilers = False,
                    critic = user
            )

            review_data = {
                'stars': 10,
                'review': 'O Poderoso Chefão 2 podia ter dado muito certo...',
                'spoilers': True,
            }

            response = self.client.put(f'/api/movies/{movie.id}/review/', review_data)

            self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


        def test_update_review_missing_arguments(self):
            critic = User.objects.create_user(
                username = 'critic',
                password = '1234',
                first_name = 'John',
                last_name = 'Cena',
                is_superuser = False,
                is_staff = True
            )

            token = Token.objects.create(user=critic)

            self.client.credentials(HTTP_AUTHORIZATION=f'Token {token.key}')

            movie = Movie.objects.create(
                title = 'O Poderoso Chefão 3',
                duration = '175m',
                genres = [
                    {'name': 'Crime'},
                    {'name': 'Drama'},
                    {'name': 'Mafia'},
                ],
                premiere = '1972-09-10',
                classification = 14,
                synopsis = 'Don Vito Corleone (Marlon Brando) é o chefe de uma "família" ...'
            )

            review = Review.objects.create(
                    stars = 7,
                    review = 'O Poderoso Chefão 2 podia ter dado muito errado...',
                    spoilers = False,
                    critic = critic
            )

            review_data = {
                'review': 'O Poderoso Chefão 2 podia ter dado muito certo...',
                'spoilers': True,
            }

            response = self.client.put(f'/api/movies/{movie.id}/review/', review_data)

            review.refresh_from_db()

            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    class UserModelTest(TestCase):
        @classmethod
        def setUpTestData(cls) -> None:
            cls.username = 'user'
            cls.password = '1234'
            cls.first_name = 'John'
            cls.last_name = 'Wick'
            cls.is_superuser = False
            cls.is_staff = False
        
            cls.user = User.objects.create(
                username=cls.username,
                password=cls.password,
                first_name=cls.first_name,
                last_name=cls.last_name,
                is_superuser=cls.is_superuser,
                is_staff=cls.is_staff
            )


        def test_user_fields(self):
            self.assertIsInstance(self.user.username, str)
            self.assertEqual(self.user.username, self.username)

            self.assertIsInstance(self.user.password, str)

            self.assertIsInstance(self.user.first_name, str)
            self.assertEqual(self.user.first_name, self.first_name)

            self.assertIsInstance(self.user.last_name, str)
            self.assertEqual(self.user.last_name, self.last_name)

            self.assertIsInstance(self.user.is_superuser, bool)
            self.assertEqual(self.user.is_superuser, self.is_superuser)

            self.assertIsInstance(self.user.is_staff, bool)
            self.assertEqual(self.user.is_staff, self.is_staff)

    class UserViewTest(APITestCase):
        def test_create_user(self):
            user_data = {
                'username': 'user',
                'password': '1234',
                'first_name': 'John',
                'last_name': 'Wick',
                'is_superuser': False,
                'is_staff': False,
            }


            response = self.client.post('/api/accounts/', user_data)

            created_user = User.objects.get(username=user_data['username'])

            self.assertEqual(response.status_code, status.HTTP_201_CREATED)

            self.assertEqual(response.json()['username'], 'user')

            self.assertNotIn('password', response.json())

            for k, v in user_data.items():
                self.assertEquals(v, response.data[k])
                self.assertEquals(v, getattr(created_user, k))


        def test_create_user_username_already_exists(self):
            user = User.objects.create_user(
                username = 'user',
                password = '1234',
                first_name = 'John',
                last_name = 'Wick',
                is_superuser = False,
                is_staff = False
            )

            new_user_data = {
                'username': 'user',
                'password': '1234',
                'first_name': 'John',
                'last_name': 'Wick',
                'is_superuser': False,
                'is_staff': False,
            }

            response = self.client.post('/api/accounts/', new_user_data)

            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


        def test_create_user_fail(self):
            user_data = {
                'username': 'user'
            }

            response = self.client.post('/api/accounts/', user_data)

            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


        class LoginViewTest(APITestCase):
            def setUp(self):
                User.objects.create_user(
                    username = 'user',
                    password = '1234',
                    first_name = 'John',
                    last_name = 'Wick',
                    is_superuser = False,
                    is_staff = False,
                )


            def test_login(self):
                login_data = {
                    'username': 'user',
                    'password': '1234'
                }

                response = self.client.post('/api/login/', login_data)

                self.assertEqual(response.status_code, status.HTTP_200_OK)

                self.assertIn('token', response.json())


            def test_login_fail(self):
                login_data = {
                    'username': 'user',
                    'password': '4321'
                }

                response = self.client.post('/api/login/', login_data)

                self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


            def test_login_missing_fields(self):
                login_data = {
                    'username': 'user'
                }

                response = self.client.post('/api/login/', login_data)

                self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
