from django.db import models

class Movie(models.Model):

    title = models.CharField(max_length=255, unique=True)
    duration = models.CharField(max_length=255)
    premiere = models.CharField(max_length=255)
    classification = models.IntegerField()
    synopsis = models.CharField(max_length=255)

class Genre(models.Model):
    name = models.CharField(max_length=255, unique=True)
    movie = models.ManyToManyField('movies.Movie', on_delete=models.CASCADE, related_name='genres')

class Review(models.Model):
    critic = models.ForeignKey('users.User', on_delete=models.CASCADE)
    stars = models.IntegerField()
    review = models.CharField(max_length=255)
    spoilers = models.BooleanField()
    movie = models.ForeignKey('movies.Movie', on_delete=models.CASCADE, related_name='reviews')
