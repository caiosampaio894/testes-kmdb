from django.http import response
from django.test import TestCase
from django.contrib.auth.models import User
from rest_framework.test import APITestCase
from rest_framework import status

class UserTestModel(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.username = 'Caio123'
        cls.fist_name = 'Caio'
        cls.last_name = 'Sampaio'
        cls.password = '123456'
        cls.is_staff = True
        cls.is_superuser = True

        cls.user = User.objects.create(
            username = cls.username,
            first_name = cls.first_name,
            last_name = cls.last_name,
            password = cls.password,
            is_staff = cls.is_staff,
            is_superuser = cls.is_superuser
        )

    def test_user_has_information_fields(self):
        self.assertIsInstance(self.user.username, str)
        self.assertEqual(self.user.username, self.username)

        self.assertIsInstance(self.user.first_name, str)
        self.assertEqual(self.user.first_name, self.first_name)

        self.assertIsInstance(self.user.last_name, str)
        self.assertEqual(self.user.last_name, self.last_name)

        self.assertIsInstance(self.user.password, str)
        self.assertEqual(self.user.password, self.password)

        self.assertIsInstance(self.user.is_staff, bool)
        self.assertEqual(self.user.is_staff, self.is_staff)

        self.assertIsInstance(self.user.is_superuser, bool)
        self.assertEqual(self.user.is_superuser, self.is_superuser)


class UserTestView(APITestCase):
    
    def test_create_account(self):
        url_create = f"/api/accounts"
        account_data = {
            "username": "Caio123",
            "first_name": "Caio",
            "last_name": "Sampaio",
            "password": '123456',
            "is_staff": True,
            "is_superuser": True
        }
        response = self.client.post(url_create, account_data)

        created_user = User.objects.get(username=account_data["username"])

        self.assertEqual(response.status_code, 201)


        for k, v in account_data.items():
            self.assertEqual(v, response.data[k])
            self.assertEqual(v, getattr(created_user, k))


    def test_create_account_fail(self):
        url_create = f"/api/accounts"
        account_data = {
            'username': 'user'
        }

        response = self.client.post(url_create, account_data)

        self.assertEqual(response.status_code, 400)
        

class LoginTestView(APITestCase):
    def setUp(self):
        user =  User.objects.create_user(
            username='Caio123',
            password='123456'
        )
    
    def test_login_sucess(self):
        login_data = {
            'username': 'Caio123',
            'password': '123456'
        }

        response = self.client.post('api/login', login_data)

        self.assertEqual(response.status_code, 200)
        self.assertIn('token', response.json())

    def test_login_error(self):
        login_data = {
            'username': 'Caio123',
            'password': '1'
        }

        response = self.client.post('api/login', login_data)

        self.assertEqual(response.status_code, 400)